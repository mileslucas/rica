import os
import sys
import argparse
import numpy as np
from jinja2 import Environment, PackageLoader, select_autoescape
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
# Must import after mpl.use
import mpld3
style = {
	'axes.labelsize': 14,
	'axes.titlesize': 16,
	'legend.fontsize': 14,
	'figure.figsize': (4, 3)
}
mpl.rcParams.update(style)
plt.style.use('seaborn')

base = os.path.dirname(__file__)
sys.path.append(base + '/../src/')
import _models as m

def gen_doc(model_names=m.models.keys()):
	env = Environment(
		loader=PackageLoader('gen', 'templates'),
		autoescape=select_autoescape(['html', 'xml']),
	)
	env.globals.update(zip=zip)

	template = env.get_template('report.html')

	results_files = os.listdir(base + '/static/images/results')
	diags_files = os.listdir(base + '/static/images/diags')
	ratios_files = os.listdir(base + '/static/ratios')
	ratios_plots_files = list(map(ratio_plot, ratios_files))
	chi2_files = os.listdir(base + '/static/chi2')
	chi2_plots_files = list(map(chi2_plot, chi2_files))
	
	results = {}
	diags = {}
	ratios = {}
	ratio_plots = {}
	chi2 = {}
	chi2_plots = {}
	for mod in model_names:
		results[mod] = sorted([f for f in results_files if f.startswith(mod + '_')])
		diags[mod] = sorted([f for f in diags_files if f.startswith(mod + '_')])
		ratios[mod] = sorted([f for f in ratios_files if f.startswith(mod + '_')])
		ratio_plots[mod] = sorted([f for f in ratios_plots_files if f.startswith(mod + '_')])
		chi2[mod] = sorted([f for f in chi2_files if f.startswith(mod + '_')])
		chi2_plots[mod] =	 sorted([f for f in chi2_plots_files if f.startswith(mod + '_')])
		
	context = {
		'models': model_names,
		'results': results,
		'diags': diags,
		'ratios': ratios,
		'ratio_plots': ratio_plots,
		'chi2': chi2,
		'chi2_plots': chi2_plots,
		'params': m.models
	}

	outfile = base + '/index.html'

	template.stream(context).dump(outfile)


def ratio_plot(filename, savename=None):
	"""
	Create a plot comparing the ratios of different combination methods
	
	Parameters
	----------
	filename : str
		The csv file to construct the plot from
	savename : str, optional
		The filename to save the image to (Default is None)
	"""

	uv, feather_ratio, smclean_ratio, int_ratio, joint_ratio = np.genfromtxt(base + '/static/ratios/' + filename, unpack=True, delimiter=',', usecols=[0,3,6,9,12])
	kuv = uv / 1000
	plt.figure()
	for r in [feather_ratio, smclean_ratio, int_ratio, joint_ratio]:
		plt.plot(kuv, r)
	plt.plot(kuv, np.ones_like(kuv), ls='--', c='0.4', zorder=1)
	upper = kuv[10]
	plt.xlim(0 - 0.1 * (upper - kuv[0]), upper + 0.1 * (upper - kuv[0]))

	mask = kuv < upper
	low_lim = np.min([feather_ratio[mask], smclean_ratio[mask], int_ratio[mask], joint_ratio[mask]])
	high_lim = np.max([feather_ratio[mask], smclean_ratio[mask], int_ratio[mask], joint_ratio[mask]])
	rng = high_lim - low_lim
	# some images cause issues, just bypass them
	try:
		plt.ylim(low_lim - 0.1*rng, high_lim + 0.1*rng)
	except:
		pass

	plt.legend(['feather', 'smclean', 'int', 'joint'])
	plt.xlabel(r'UV ($k\lambda$)')
	plt.ylabel('Ratio')
	plt.tight_layout()
	
	if savename is None:
		savename = filename.replace('csv', 'html')

	if not os.path.exists(base + '/templates/ratio_plots'):
		os.system('mkdir {}/templates/ratio_plots'.format(base))
	mpld3.save_html(plt.gcf(), base + '/templates/ratio_plots/' + savename)

	plt.close()
	return savename

def chi2_plot(filename, savename=None):
	methods = ['feather', 'smclean', 'int', 'joint']
	image_chi2 = np.ravel(np.genfromtxt('docs/static/chi2/' + filename, delimiter=','))
	# ignore tp2vis
	image_chi2 = image_chi2[:-1]
	cells = range(len(image_chi2))
	plt.figure()
	plt.bar(cells, image_chi2)
	plt.xticks(cells, methods)

	plt.ylabel(r'$\chi^2$')
	plt.tight_layout()
	
	if savename is None:
		savename = filename.replace('csv', 'html')
	if not os.path.exists(base + '/templates/chi2_plots'):
		os.system('mkdir {}/templates/chi2_plots'.format(base))
	mpld3.save_html(plt.gcf(), base + '/templates/chi2_plots/' + savename)

	plt.close()
	return savename


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	ratios_files = os.listdir(base + '/static/ratios')
	models = [filename.split('__')[0] for filename in ratios_files]
	model_names = sorted(set(models))
	print('Models: {}'.format('\n'.join(model_names)))
	gen_doc(model_names)
