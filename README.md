# RICA

**R**adio **I**maging **C**ombination **A**nalysis

RICA is a tool to measure the effectiveness of different methods of combining single dish radio data and interferometer radio data. This is important because interferometer data is missing the 0-spacing total power from its observations, which can only be retrieved by single dish telescopes. 

The current combination methods being tested are CASA's `feather`, using the total power image (TPI) as a starting model in CASA's `tclean`, performing a joint deconvolution with the TPI in a modified Cotton-Schwab CLEAN algorithm, and [tp2vis](https://github.com/tp2vis/distribute), which creates a measurement set from the TPI. The `combine.py` script has all the tools to create a combined image from each method given a visibility, a pre-cleaned image, and a TPI. 

In order to analyze the results there are several diagnostics. When comparing against a known model, the fidelity image is a great tool for measuring accuracy. Also, residuals from cleaning help show any errors in the clean parameters. Our main source of comparison, though, is a ratio of the power spectrum density (PSD) of the model image and the test image. This ratio should be as close to 1.0 as possible at 0 UV spacing. If the model is not known, a comparison can be done between the TPI and the test image, where the ratio should still be close to 1.0 at 0 UV spacing, indicating the total power from the TPI was correctly combined. All of these comparisons can be facilitated by the `compare.py` script, outlined below. 

## Prerequisites

This code relies on NRAO's [CASA](https://casa.nrao.edu/). You can download and install CASA following their instructions or, if you are running Docker you can use the CASA docker images provided in this registry

#### Docker

I assume basic knowledge of Docker. For Linux, all you have to run is
```
$ docker run -ti -e DISPLAY=$DISPLAY -v ~/your/local/codebase:/code registry.gitlab.com/mileslucas/rica
```
On Windows, you will first need to get an X windows server, do your own research. Once the server is started, note its address. I will assume it is `localhost:0.0`. To forward this to Docker, you need to know your own local IP address. Use `ipconfig` to determine this. Now, using Windows Powershell (will not work on git bash or other non-TTY terminals) execute:
```
> docker run -ti -e DISPLAY=<your local ip address>:0.0 -v c:/your/local/codebase:/code registry.gitlab.com/mileslucas/rica
```

Once docker is started, you should have a casa terminal and logger. If you don't see the logger, you have not forwarded your display correctly. within CASA, you can `cd /code` to execute code on the files mounted from the local filesystem. In my experience, I had issues running CASA scripts with files that already existed (ie `data/RXJ1347.model`) but had no problem creating new models. 

## Scripts

**Compare**

Creates comparison data between two images based on the radial average of their Fourier transform.

```
CASA <1>: run src/compare.py -h
usage: compare.py [-h] [--no-plot] [--no-fidelity] project reference image

Compare the two images by getting the PSD, binning them, and getting the ratio
of the two. Ideally this raito should be 1.0 near the 0 uv point

positional arguments:
  project        path to the folder containing the images
  reference      path to the reference image
  image          path to the test image

optional arguments:
  -h, --help     show this help message and exit
  --no-plot      Does not plot the final output. Useful if no X11 display
  --no-fidelity  Do not calculate and plot fidelity.
```

Basic example:
```
CASA<1>: run compare.py data orion.gbt.im orion.gbt.noisy.im
```

**Simulate**

This will create simulated data for single dish and intereferometer modes using `simobserve`.

```
CASA <1>: run src/simulate.py -h
usage: simulate.py [-h] [-f] project name model

positional arguments:
  project      The path for storing all simulated files
  name         The filename prefix
  model        The model for image creation. One of ['ppd', 'RXJ1347',
               'gauss', 'm51-ms', 'orion-c', 'orion-b', 'm51', 'orion-b-25',
               'points', 'combined', 'orion-b-ms']

optional arguments:
  -h, --help   show this help message and exit
  -f, --force  Will force recreate the truth images for the simulations
```

**Combine**

This will take simulated data and combine it. Currently it combines using the `feather` task, by using a `startmodel` in `tclean`, by joint deconvolving the single dish and interferometer image, and by using the [tp2vis](https://github.com/tp2vis/distribute) script.

```
CASA <1>: run src/combine.py -h
usage: combine.py [-h] project vis highres lowres

positional arguments:
  project     The base directory for the files
  vis         The MS for the visibility data
  highres     The cleaned interferometer data
  lowres      The single dish image

optional arguments:
  -h, --help  show this help message and exit
```

**Pipeline**

This will create every image and combination from all possible models.

```
CASA <1>: run src/pipeline.py -h
usage: pipeline.py [-h] [-m MODELS [MODELS ...]] [--no-simulate]
                   [--compare-only] [-p]
                   project

positional arguments:
  project               The project folder.

optional arguments:
  -h, --help            show this help message and exit
  -m MODELS [MODELS ...], --models MODELS [MODELS ...]
                        The models to run, out of ['ppd', 'RXJ1347', 'gauss',
                        'm51-ms', 'orion-c', 'orion-b', 'm51', 'orion-b-25',
                        'points', 'combined', 'orion-b-ms']
  --no-simulate         Only run comparison scrips (no simulation)
  --compare-only
  -p, --parallel        run clean in parallel

```


## Tests

In order to run tests, the CASA python needs to be located and unittest must be run. For instance, my CASA python is located at `/home/casa/packages/RHEL6/release/current/bin/python`. 

To run the unittests 
```
$ <CASA python bin> -m unittest discover -v
```

This will run a test suite runner for all scripts. If a specific script is to be tested, use the explicit test script for that
```
$ <CASA python bin> -m unittest -v tests.test_<script>
```
## Acknowledgements

This work was funded by the National Science Foundation in partnership with the National Radio Astronomy Observatory and Associated Universities Incorporated. Thank you to Dr. Kumar Golap and Dr. Takahiro Tsutsumi for their guidance and assistance. 
