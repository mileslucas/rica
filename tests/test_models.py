import unittest
from src import _models as m

class TestHelpers(unittest.TestCase):

	def test_prime_factors_1(self):
		expected = [2,]
		self.assertEqual(m.get_prime_factors(512), expected)

	def test_prime_factors_2(self):
		expected = [1,]
		self.assertEqual(m.get_prime_factors(1), expected)

	def test_prime_factors_3(self):
		expected = [2, 3, 7]
		self.assertEqual(m.get_prime_factors(2268), expected)

	def test_prime_factors_4(self):
		expected = [17,]
		self.assertEqual(m.get_prime_factors(17), expected)

	def test_prime_factors_5(self):
		expected = [2, 2, 2]
		self.assertEqual(m.get_prime_factors(8, False), expected)
		
	def test_imsize_1(self):
		num = 2262
		expected = 2268
		self.assertEqual(m.get_imsize(num), expected)

	def test_imsize_2(self):
		num = 256
		expected = 256
		self.assertEqual(m.get_imsize(num), expected)

	def test_imsize_3(self):
		num = 2269
		self.assertTrue(m.get_imsize(num) > 2268)
		
	def test_imsize_float(self):
		num = 128.79
		expected = 128
		self.assertEqual(m.get_imsize(num), expected)

	def test_merge(self):
		a = {'a':1, 'b':2}
		b = {'c':3, 'd':4}
		expected = {'a':1, 'b':2, 'c':3, 'd':4}
		self.assertEqual(set(m.merge(a, b)), set(expected))

class TestModels(unittest.TestCase):

	def test_models_params(self):
		bools = []
		for mod in m.models.values():
			bools.append('params' in mod)
		self.assertTrue(all(bools))

	def test_models_imsize_and_cell(self):
		bools = []
		for mod in m.models.values():
			for p in mod['params']:
				bools.append('imsize' in p and 'cell' in p)