import unittest
import numpy as np

try:
	from src import compare as c
except:
	pass

@unittest.skip('Unresolved issues importing CASA')
class TestRatio(unittest.TestCase):

	def setUp(self):
		self.matrix = np.random.rand(100, 100)

	def test_get_psd(self):
		psd = c.radial_profile(self.matrix)
		self.assertEqual(len(psd), 50)

	def test_get_psd_sum(self):
		psd = c.radial_profile(self.matrix)
		expected = np.sum(self.matrix)
		self.assertTrue(np.isclose(psd[0], expected))
