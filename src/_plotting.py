"""
_plotting.py

Miles Lucas - mdlucas@nrao.edu
"""

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.patches import Ellipse
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np

from casac import casac
ia = casac.image()
qa = casac.quanta()

from _cm import cmaps

style = {
	'axes.labelsize': 14,
	'axes.titlesize': 16,
	'legend.fontsize': 14,
	'image.cmap': cmaps['viridis']
}
mpl.rcParams.update(style)

def plot_image(image, ax=None, plot_stats=False, plot_rb=False):
	"""
	Plots a sky image
	
	Parameters
	----------
	image : str
		The path to the image
	ax : Matplotlib.Axes, optional
		The axis to plot the image on (the default is None, which will create a new axis)
	plot_stats : bool, optional
		If True, will plot statistics in place in box in top left of image (the default is False)
		plot_rb : bool, optional
				If True, will plot the restoring beam in the bottom left corner
	
	Returns
	-------
	Matplotlib.Axes
		The plotted image
	"""

	ia.open(image)
	cs = ia.coordsys()
	try:
		# Get the image data
		coords = [cs.findcoordinate('stokes')['pixel'][0], cs.findcoordinate('spectral')['pixel'][0]]
		data = ia.getchunk(axes=coords, dropdeg=True)
		# Get the extents
		dir_axes = cs.findcoordinate('direction')['pixel']
		ref = cs.referencepixel()['numeric'][dir_axes]
		stats = ia.statistics()
		# For some reason cs.names does not return np array so cannot index with list
		names = np.array(cs.names())[dir_axes]

		ext = (
			stats['blc'][0] - ref[0],
			stats['trc'][0] - ref[0],
			stats['blc'][1] - ref[1],
			stats['trc'][1] - ref[1]
		)

		# Plot the image
		if ax is None:
			fig, ax = plt.subplots()

		im = ax.imshow(data.T, origin='lower', aspect='equal', extent=ext, interpolation=None)
		colorbar(im)
		ax.set_xlabel(names[0] + ' (px)')
		ax.set_ylabel(names[1] + ' (px)')
	
		if plot_stats:
			props = dict(boxstyle='round', facecolor='lightgrey', alpha=0.5)
			string = '$\mu$={:.2e}\nrms={:.2e}\nmin={:.2e}\nmax={:.2e}'.format(
				stats['mean'][0], stats['rms'][0], stats['min'][0], stats['max'][0])
			ax.text(0.05, 0.95, string, transform=ax.transAxes, fontsize=14, verticalalignment='top', bbox=props)
			if plot_rb:
				rb = ia.restoringbeam()
				inc = abs(float(cs.increment('s')['string'][0].split()[0]))
				major = rb['major']['value'] / inc
				minor = rb['minor']['value'] / inc
				xy = np.array([ext[0], ext[2]]) + (ext[1] - ext[0]) / 10
				e = Ellipse(xy, major, minor, -rb['positionangle']['value'], ec='w', fc='none')
				ax.add_artist(e)
				e.set_clip_box(ax.bbox)
		
						
	finally:
		ia.close()
		cs.done()

	return ax


def plot_fft(image, ax=None):
	"""
	Plots an FFT image
	
	Parameters
	----------
	image : str
		The path to the fft image
	ax : Matplotlib.Axes, optional
		If provided, the axes to plot on (the default is None)
	
	Returns
	-------
	Matplotlib.Axes
		The plotted image
	"""

	ia.open(image)
	cs = ia.coordsys()
	try:
		coords = [cs.findcoordinate('stokes')['pixel'][0], cs.findcoordinate('spectral')['pixel'][0]]
		data = ia.getchunk(axes=coords, dropdeg=True)
	
		# Get the extents
		lin_axes = cs.findcoordinate('linear')['pixel']
		# For some reason cs.names does not return np array so cannot index with list
		names = np.array(cs.names())[lin_axes]

		
		if ax is None:
			fig, ax = plt.subplots()
			
		blc = cs.toworld([0, 0])['numeric'][lin_axes]/1000
		trc = cs.toworld(ia.shape()[lin_axes])['numeric'][lin_axes]/1000
		ext = (
			blc[0],
			trc[0],
			blc[1],
			trc[1]
		)
		
		im = ax.imshow(data.T, origin='lower', aspect='equal', extent=ext)
		colorbar(im)
		ax.set_xlabel(names[0] + r' ($k \lambda$)')
		ax.set_ylabel(names[1] + r' ($k \lambda$)')

	finally:
		ia.close()
		cs.done()

	return ax

def colorbar(mappable, ticks=True):
	"""
	Creates a colorbar instance that is tight to the image and maintains the aspect ratio
	
	Parameters
	----------
	mappable : Matplotlib mappable
		A mappable object
		ticks : bool, optional
				If True, will plot the labels for the colorbar (the default is True)
	Returns
	-------
	Matplotlib.ArtistObject
		The colorbar for the given mappable
	"""

	ax = mappable.axes
	fig = ax.figure
	divider = make_axes_locatable(ax)
	cax = divider.append_axes("right", size="5%", pad=0.)
	cbar = fig.colorbar(mappable, ax=ax, cax=cax, format='%1.1e')
	if not ticks:
		cbar.ax.set_yticklabels([])
		cbar.set_ticks([])
	return cbar

