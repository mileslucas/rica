'''
_joint_deconvolve.py

Miles Lucas - mlucas@nrao.edu
Kumar Golap - kgolap@nrao.edu
'''

import os

from tclean import tclean
from feather import feather
from imregrid import imregrid
from casac import casac
dc=casac.deconvolver()
ia=casac.image()
cl=casac.componentlist()

def joint_deconvolve(vis, sdimage, imagename, imsize=[], cell='', niter=1000, nmajor=10, deconvolver='hogbom', scales=[], threshold='0Jy', **extra_args):
	"""
	Does a joint deconvolution of a single dish image and interferometer image This differs
	from using a starting model by incorporating the single dish image in the Cotton-Schwab 
	major cycle
	
	Parameters
	----------
	vis : str
		The path to the visibility MS
	sdimage : str
		The path to the total power image
	imagename : str
		The imagename for cleaning
	imsize : int or list, optional
		The image size in pixels. If int provided, will be a square image
		 (the default is [])
	cell : str, optional
		The cell size for the pixels (the default is '')
	niter : int, optional
		The number of minor cycle iterations (the default is 1000)
	nmajor : int, optional
		The number of major cycle iterations (the default is 10)
	deconvolver : str, optional
		The deconvolver used for cleaning. Can be one of 'hogbom', 'fullmsclean', 
		and 'multiscale' (the default is 'hogbom')
	scales : list, optional
		If provided, the scales for multiscale cleaning (the default is [])
	threshold : str, optional
		The threshold for cleaning, if provided (the default is '0Jy')
	
	"""


	if deconvolver == 'multiscale':
		deconvolver = 'fullmsclean'
	elif deconvolver == 'hogbom':
		deconvolver = 'fullmsclean'
		scales = [0]
	cleanname = imagename + '.tmp'
	os.system('rm -rf {}'.format(cleanname))
	tclean(vis=vis, niter=0, imsize=imsize, cell=cell, imagename=cleanname, calcpsf=True, calcres=True, threshold=threshold)
	ia.open(sdimage)
	sdbeam=ia.restoringbeam()
	ia.done()
	ia.open(cleanname + '.image')
	intbeam=ia.restoringbeam()
	ia.done()
	# make a joint psf
	ia.fromimage(outfile='sd.psf', infile=cleanname+'.image', overwrite=True)
	ia.set(0)
	if isinstance(imsize, int):
		imsize = [imsize, imsize]
	center=ia.toworld([imsize[0]/2,imsize[1]/2,0,0], 'm')['measure']['direction']
	cl.done()
	cl.addcomponent(dir=center, flux=1.0, shape='Gaussian', majoraxis=sdbeam['major'], minoraxis=sdbeam['minor'], positionangle=sdbeam['positionangle'])
	ia.modify(cl.torecord(), subtract=False)
	ia.calc('"sd.psf"/max("sd.psf")')
	ia.setrestoringbeam(major='', minor='', pa='', beam=sdbeam)
	ia.done()
	feather(imagename + '.joint.psf', highres=cleanname+'.psf', lowres='sd.psf')

	mod=cleanname+'.model'
	newsdimage=sdimage+'.regrid'
	imregrid(imagename=sdimage, template=mod, output=newsdimage, overwrite=True)
	newmask=''
	if('mask' in extra_args):
		newmask=extra_args['mask']+'.regrid'
		imregrid(imagename=extra_args['mask'], template=mod, output=newmask, overwrite=True)
	sdresidual=newsdimage
	loopniter=niter/nmajor
	for k in range(nmajor):
		# the residual does not have a beam
		ia.open(cleanname+'.residual')
		ia.setrestoringbeam(major='', minor='', pa='', beam=intbeam)
		ia.done()
		# feather the interf residual and the sd residual
		feather(imagename + '.joint.residual', highres=cleanname+'.residual', lowres=sdresidual)
		# deconvolve a bit and update the  clean model image
		dc.open(imagename + '.joint.residual', imagename + '.joint.psf')
		dc.setscales(scalemethod='uservector', uservector=scales)
		dc.clean(algorithm=deconvolver, niter=loopniter,  model=mod, threshold=threshold, mask=newmask)
		dc.done()
		# make a new  interf residual
		tclean(vis=vis, niter=0, imsize=imsize, cell=cell, imagename=cleanname, calcpsf=False, calcres=True, threshold=threshold)
		# make a new Sd residual
		ia.open(cleanname+'.model')
		ib=ia.convolve2d(outfile='sd.residual',  major=sdbeam['major'], minor=sdbeam['minor'], pa=sdbeam['positionangle'], overwrite=True)
		ia.done()
		ib.calc('"{}" - "sd.residual"'.format(newsdimage))
		ib.done()
		sdresidual='sd.residual'
	# end of loop
	
	feather(imagename + '.joint.residual', highres=cleanname+'.residual', lowres=sdresidual)
	
	ia.open(cleanname+'.model')
	ib=ia.convolve2d(outfile=imagename + '.joint.image',  major=intbeam['major'], minor=intbeam['minor'], pa=intbeam['positionangle'], overwrite=True)
	ia.done()
	ib.calc('"{0}.joint.image"+"{0}.joint.residual"'.format(imagename))
	ib.done()
	os.system('rm -rf sd.psf')
	os.system('rm -rf sd.residual')
	os.system('rm -rf {}*'.format(cleanname))
	
