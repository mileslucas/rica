"""
combine.py

Miles Lucas - mdlucas@nrao.edu
"""

import argparse
import os
import sys

# CASA imports
from listobs import listobs
from imstat import imstat
from concat import concat
from casac import casac
from feather import feather
im = casac.imager()

# Local imports
from simulate import clean
from _joint_deconvolve import joint_deconvolve
from _tp2vis import tp2vis, tp2viswt

def get_name(imagename):
	"""
	Gets the image base given an image name
	
	Parameters
	----------
	imagename : str
		The path of a given image

	Returns
	-------
	str
		The image base. E.G. if passed 'sim/comb.int.image' will return 'sim/comb
	"""

	tokens = imagename.split('.')
	if 'sd' in tokens:
		return imagename.split('sd')[0][:-1]
	elif 'int' in tokens:
		return imagename.split('int')[0][:-1]
	else:
		return '.'.join(tokens[:-1])


def feather_comb(basename, highres, lowres, **extra_params):
	"""
	Combines the images using the feather task
	
	Parameters
	----------
	basename : str
		The basename for saving the file
	highres : str
		The path to the cleaned interferometry image
	lowres : str
		The path to the SD image
	extra_params : dict, optional
		Any extra parameters to be passed to feather

	Returns
	-------
	str
		The full path of the outputted feather image
	"""

	outname = basename + '.feather.image'
	feather(outname, highres, lowres, **extra_params)
	return outname

def sm_clean(basename, visibility, lowres, **extra_params):
	"""
	Combines the images by using the lowres image as the starting model for cleaning
	the visiblity data
	
	Parameters
	----------
	basename : str
		The basename for saving the files
	visibility : str
		The path to the MS
	lowres : str
		The path to the SD image
	extra_params : dict, optional
		Any extra parameters to be passed to clean

	Returns
	-------
	str
		The full path of the cleaned image
	"""

	outname = basename + '.smclean'
	# Clean up previous instances since we can't overwrite
	os.system('rm -rf {}*'.format(outname))
	# Need to create model from sd data to fix bug in tclean where values are not scaled
	# properly into Jy/pixel
	im.open(visibility)
	im.defineimage()
	model_name = '.'.join(lowres.split('.')[:-1]) + '.beam.model'
	im.makemodelfromsd(lowres, model_name)
	im.close()

	clean(visibility, imagename=outname, startmodel=model_name, **extra_params)
	os.system('rm -rf {}'.format(model_name))

	return outname + '.image'

def jd(basename, visibility, lowres, **params):
	os.system('rm -rf {}*'.format(basename + '.joint'))
	joint_deconvolve(visibility, lowres, basename, **params)

	return basename + '.joint.image'


def tp2v(basename, visibility, lowres, **extra_params):
	"""
	Wraps the tp2vis script from _tp2vis https://github.com/tp2vis/distribute
	
	Parameters
	----------
	basename : str
		The base name of the model e.g. 'sim/points'
	visibility : str
		The path to the visibility MS
	lowres : str
		The path to the total power image
	extra_params : dict, optional
		Any extra parameters to be passed to clean
		
	Returns
	-------
	str
		The path to the cleaned image
	"""
	# Get the pointing file
	tmp_file = 'temp.ptg'
	ptg_file = basename + '.ptg'
	listobs(visibility, listfile=tmp_file, overwrite=True)
	with open(tmp_file, 'r') as f:
		lines = f.readlines()

	nfields = None
	fields = None
	rf = None
	for i, line in enumerate(lines):
		if line.startswith('Fields: '):
			nfields = int(line.split()[1])
			fields = lines[i+2:i+2+nfields]
		if line.startswith('Sources: '):
			rf = lines[i+2].split()[3]

	pointings = []
	for field in fields:
		tokens = field.split()
		ptg = [tokens[i] for i in [5, 3, 4]]
		pointings.append(' '.join(ptg))

	with open(ptg_file, 'w+') as f:
		f.writelines(pointings)

	os.system('rm -rf {}'.format(tmp_file))

	# Do tp2vis
	rms = imstat(lowres, axes=[0,1])['rms'].mean()
	tpvis = basename + '.sd.ms'
	tp2vis(lowres, tpvis, ptg_file, rms=rms)
	comb_ms = basename + '.all.ms'
	if os.path.exists(comb_ms):
		os.system('rm -rf {}'.format(comb_ms))
	tp2viswt([visibility, tpvis], mode='rms', value=6.0)
	concat(vis=[visibility, tpvis], concatvis=comb_ms, copypointing=False)
	
	# Clean
	extra_params.update(dict(restfreq='{}MHz'.format(rf)))
	clean_name = basename + '.tp2vis'
	if os.path.exists(clean_name + '.image'):
		os.system('rm -rf {}.*'.format(clean_name))
	
	clean(comb_ms, imagename=clean_name, **extra_params)

	return clean_name + '.image'
	
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('project', help='The base directory for the files')
	parser.add_argument('vis', help='The MS for the visibility data')
	parser.add_argument('highres', help='The cleaned interferometer data')
	parser.add_argument('lowres', help='The single dish image')

	args = parser.parse_args()

	name = get_name(args.project + '/' + args.highres)
	vis = args.project + '/' + args.vis
	highres = args.project + '/' + args.highres
	lowres = args.project + '/' + args.lowres
	
	feather_comb(name, highres, lowres)
	sm_clean(name, vis, lowres)
	jd(name, vis, lowres)
	tp2v(name, vis, lowres)
	


