import sys
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse, Circle, Patch
import numpy as np

def make_model_figure(dir='.'):
	if dir[-1] == '/':
		dir = dir[:-1]
	from casac import casac
	ia = casac.image()
	qa = casac.quanta()
	from _plotting import colorbar

	models = [
		'sim/models/points.conv',
		'sim/models/gauss.conv',
		'sim/models/combined.conv',
		'sim/models/orion-b.conv',
		'sim/models/m51-b.conv',
		'sim/models/RXJ1347.conv',
		'data/ppd.conv'
	]
	names = [
		'Points',
		'Gaussian',
		'Combined',
		'Orion',
		'M51',
		'RXJ1347',
		'PPD'
	]
	width = 7.00
	height = width / 2 + 2 * 12 / 300
	fig, axes = plt.subplots(2,4, figsize=(width, height))
	fig.delaxes(axes[1][3])
	for i, (image, ax, name) in enumerate(zip(models, axes.flat, names)):
		ia.open(image)
		cs = ia.coordsys()
		# Get the image data
		coords = [cs.findcoordinate('stokes')['pixel'][0], cs.findcoordinate('spectral')['pixel'][0]]
		data = ia.getchunk(axes=coords, dropdeg=True)
	
		# Plot the image
		im = ax.imshow(data.T, origin='lower', aspect='equal', interpolation=None)
		if i < 4:
				ax.set_title(name, fontname='Bitstream Vera Sans', fontsize=10)
		else:
				ax.set_xlabel(name, fontname='Bitstream Vera Sans', fontsize=10)
		ax.set_xticks([])
		ax.set_yticks([])
		colorbar(im, False)
		rb = ia.restoringbeam()
		inc = abs(float(cs.increment('s')['string'][0].split()[0]))
		major = rb['major']['value'] / inc
		minor = rb['minor']['value'] / inc
		y, x = data.shape
		xy = np.array([x, y]) / 10
		e = Ellipse(xy, major, minor, -rb['positionangle']['value'], ec='w', fc='none')
		ax.add_artist(e)
		e.set_clip_box(ax.bbox)
		
		ia.close()

	plt.tight_layout()        
	plt.subplots_adjust(hspace=0.0, wspace=-0.2)
	plt.savefig(dir + '/models.pdf', dpi=300)


def make_uv_plots(dir='.'):
	if dir[-1] == '/':
		dir = dir[:-1]
	from plotuv import plotuv
	files = [
		'sim/combined.int.ms',
		'sim/combined.sd.ms',
		'sim/combined.all.ms'
	]
	names = [
		'Interferometer Visibilities',
		'SD Visibilities',
		'Combined Visibilities'
	]
	for f, n in zip(files, names):
		plt.figure(figsize=(5, 5))
		plotuv(f, maxnpts=99999999999, colors='b')
		plt.xlim(-1, 1)
		plt.ylim(-1, 1)
		plt.title(n, fontsize=16)
		plt.xlabel(r'U (k$\lambda$)', fontsize=14)
		plt.ylabel(r'V (k$\lambda$)', fontsize=14)
		sub = f.split('.')[-2]
		plt.tight_layout()
		plt.savefig(dir + '/' + sub + '-vis.pdf', dpi=300)
		plt.close()


def make_sd_plots(dir='.'):
	import pandas as pd
	plt.style.use('seaborn-paper')
	if dir[-1] == '/':
		dir = dir[:-1]
	files = [
		'docs/static/ratios/orion-b__model.csv',
		'docs/static/ratios/orion-b-25__model.csv'
	]
	sizes = [100, 25]
	names = ['feather_ratio', 'smclean_ratio', 'int_ratio', 'joint_ratio']
	fig, axes = plt.subplots(1, 2, figsize=(5, 3), sharex=True, sharey=True)
	axes[0].set_ylabel('Power Ratio', fontsize=10)
	[ax.set_xlabel(r'UV dist(k$\lambda$)', fontsize=10) for ax in axes]

	for i, f in enumerate(files):
		data = pd.read_csv(f)
		for n in names:
			axes[i].plot(data['# uv'] / 1000, data[n], label=n.split('_')[0])
		axes[i].axhline(1.0, ls='--', c='k', zorder=0)
		axes[i].set_title('Orion - {} m SD'.format(sizes[i]), fontsize=12)

	axes[0].legend()
	plt.tight_layout()
	plt.subplots_adjust(wspace=0)
	axes[0].set_xlim(-0.1, 1.1)
	axes[0].set_ylim(0.3, 2.3)
	plt.savefig(dir + '/sd_comp.pdf' , dpi=300)
	plt.close()

def make_sd_uv(dir='.'):
	from plotuv import plotuv
	if dir[-1] == '/':
		dir = dir[:-1]
	# UV coverage
	plt.figure(figsize=(3,3))
	radii = [100 / 0.06 / 2000, 25 / 0.06 / 2000]
	plotuv('sim/orion-b.int.ms', maxnpts=999999999, colors='b')
	plt.xlim(-2, 2)
	plt.ylim(-2, 2)
	plt.xlabel(r'U (k$\lambda$)', fontsize=10)
	plt.ylabel(r'V (k$\lambda$)', fontsize=10)
	plt.title('Orion - B config', fontsize=12)
        for tick in plt.gca().xaxis.get_major_ticks() + plt.gca().yaxis.get_major_ticks():
                tick.label.set_fontsize(8)
	c100 = Circle((0,0), radii[0], fc='none', hatch='/')
	c25 = Circle((0,0), radii[1], fc='none', hatch='\\')
	plt.gca().add_artist(c100)
	plt.gca().add_artist(c25)
	# Legend patches
	patch100 = Patch(ec='none', fc='none', hatch='/')
	patch25 = Patch(ec='none', fc='none', hatch='X')
	plt.legend(handles=[patch100, patch25], labels=['100 m', '25 m'])
	plt.tight_layout()
	plt.savefig(dir + '/uv_cover.png', dpi=300, bbox_inches='tight')
	plt.close()



def make_iter_plots(dir='.'):
	import pandas as pd
	plt.style.use('seaborn-paper')
	if dir[-1] == '/':
		dir = dir[:-1]
	files = [[
			'docs/static/ratios/combined-n0__model.csv',
			'docs/static/ratios/combined-n100__model.csv',
			'docs/static/ratios/combined-n1000__model.csv',
			'docs/static/ratios/combined__model.csv',
		],
		[
			'docs/static/ratios/orion-b-n0__model.csv',
			'docs/static/ratios/orion-b-n100__model.csv',
			'docs/static/ratios/orion-b-n1000__model.csv',
			'docs/static/ratios/orion-b__model.csv',
		]
	]
	names = ['feather_ratio', 'smclean_ratio', 'int_ratio', 'joint_ratio']
	niters = [0, 100, 1000, 100000]
	fix, axes = plt.subplots(2,4,figsize=(10, 5), sharex='row', sharey='row')
	
	axes[0, 0].set_xlim(-0.1, 2.1)
	axes[0, 0].set_ylim(0.4, 2.5)
	axes[1, 0].set_xlim(-0.1, 3.1)
	axes[1, 0].set_ylim(0.3, 3)
	axes[0, 0].set_ylabel('Combined Model Ratio', fontsize=10)
	axes[1, 0].set_ylabel('Orion B Model Ratio', fontsize=10)
	[ax.set_xlabel(r'UV dist (k$\lambda$)', fontsize=10) for ax in axes[1]]
	[a.axhline(1.0, ls='--', c='k', zorder=0) for a in axes.flat]
	[ax.set_title('niter: {}'.format(niter), fontsize=12) for ax, niter in zip(axes[0], niters)]
	for i, flist in enumerate(files):
		for j, (f, niter) in enumerate(zip(flist, niters)):
			data = pd.read_csv(f)
			for n in names:
				axes[i, j].plot(data['# uv'] / 1000, data[n], label=n.split('_')[0])

	axes[0, 3].legend()
	# axes[1, 0].legend()
	plt.tight_layout()
	
	plt.subplots_adjust(hspace=0, wspace=0)
	plt.savefig(dir + '/iter-plot.pdf', dpi=300)
	plt.close()

def make_ms_plots(dir = '.'):
	import pandas as pd
	plt.style.use('seaborn-paper')
	if dir[-1] == '/':
		dir = dir[:-1]
	files = [
		[
			'docs/static/ratios/m51-b__model.csv',
			'docs/static/ratios/m51-b-ms__model.csv',
		],
		[
			'docs/static/ratios/orion-b-25__model.csv',
			'docs/static/ratios/orion-b-25-ms__model.csv',
		],
	]
	names = ['feather_ratio', 'smclean_ratio', 'int_ratio', 'joint_ratio']
	titles = [['M51 B', 'M51 B Multiscale'], ['Orion B 25m', 'Orion B 25m Multiscale']]
	xlims = [(-0.1, 1.5), (-0.1, 1.3)]
	ylims = [(0.1, 2),(0.3, 3)]
	for i, flist in enumerate(files):
		fig, axes = plt.subplots(1, 2, figsize=(5, 3), sharex=True, sharey=True)
		axes[0].set_xlim(xlims[i])
		axes[0].set_ylim(ylims[i])
		axes[0].set_ylabel('Power Ratio', fontsize=10)
		[ax.set_xlabel(r'UV dist (k$\lambda$)', fontsize=10) for ax in axes]
		for j, f in enumerate(flist):
			data = pd.read_csv(f)
			ax = axes[j]
			for n in names:
				ax.plot(data['# uv'] / 1000, data[n], label=n.split('_')[0])
			ax.set_title(titles[i][j], fontsize=12)
			ax.axhline(1.0, ls='--', c='k', zorder=0)
		axes[0].legend()
		plt.tight_layout()
		plt.subplots_adjust(wspace=0)
		plt.savefig(dir + '/{}.pdf'.format(titles[i][1].replace(' ', '-')), dpi=300)
		plt.close()

def make_masked_plots(dir = '.'):
	import pandas as pd
	plt.style.use('seaborn-paper')
	if dir[-1] == '/':
		dir = dir[:-1]
	files = [
		[
			'docs/static/ratios/points__model.csv',
			'docs/static/ratios/points-masked__model.csv',
		],
		[
			'docs/static/ratios/RXJ1347__model.csv',
			'docs/static/ratios/RXJ1347-masked__model.csv',
		],
		[
			'docs/static/ratios/orion-b-unmasked__model.csv',
			'docs/static/ratios/orion-b__model.csv',
		],
	]
	names = ['feather_ratio', 'smclean_ratio', 'int_ratio', 'joint_ratio']
	titles = [['Points', 'Points Masked'], ['RXJ1347', 'RXJ1347 Masked'], ['Orion B', 'Orion B Masked']]
	xlims = [(-0.1, 3), (-0.1, 2), (-0.1, 1.2)]
	ylims = [(0.2, 3.2),(-0.1, 1.6), (0.1, 3.5)]
	for i, flist in enumerate(files):
		fig, axes = plt.subplots(1, 2, figsize=(5, 3), sharex=True, sharey=True)
		axes[0].set_xlim(xlims[i])
		axes[0].set_ylim(ylims[i])
		axes[0].set_ylabel('Power Ratio', fontsize=10)
		[ax.set_xlabel(r'UV dist (k$\lambda$)', fontsize=10) for ax in axes]
		for j, f in enumerate(flist):
			data = pd.read_csv(f)
			ax = axes[j]
			for n in names:
				ax.plot(data['# uv'] / 1000, data[n], label=n.split('_')[0])
			ax.set_title(titles[i][j], fontsize=12)
			ax.axhline(1.0, ls='--', c='k', zorder=0)
		axes[0].legend()
		plt.tight_layout()
		plt.subplots_adjust(wspace=0)
		plt.savefig(dir + '/{}.pdf'.format(titles[i][1].replace(' ', '-')), dpi=300)
		plt.close()

def make_complexity_plots(dir='.'):
	import pandas as pd
	plt.style.use('seaborn-paper')
	if dir[-1] == '/':
		dir = dir[:-1]
	files = [[
			'docs/static/ratios/combined__model.csv',
			'docs/static/ratios/gauss__model.csv',
			'docs/static/ratios/points__model.csv',
		],[
			'docs/static/ratios/m51-b__model.csv',
			'docs/static/ratios/RXj1347__model.csv',
			'docs/static/ratios/ppd__model.csv',
		]
	]
	names = ['feather_ratio', 'smclean_ratio', 'int_ratio', 'joint_ratio']
	titles = [['Combined', 'Gaussian', 'Points'], ['M51 B', 'RXJ1347', 'PPD']]
	fix, axes = plt.subplots(2,3,figsize=(10, 6))
	xlims = [
		[(-0.1, 3.1), (-0.1, 3.1), (-0.1, 3.1)],
		[(-0.1, 1.5), (-0.1, 2.0), (-10, 350)]
	]
	ylims = [
		[(0.4, 1.5), (0.4, 1.5), (0.4, 1.5)],
		[(0.2, 2.6), (-0.1, 1.6), (0.15, 1.5)]
	]
	axes[0, 0].set_ylabel('Model Ratio', fontsize=10)
	axes[1, 0].set_ylabel('Model Ratio', fontsize=10)
	[ax.set_xlabel(r'UV dist (k$\lambda$)', fontsize=10) for ax in axes[1]]
	[a.axhline(1.0, ls='--', c='k', zorder=0) for a in axes.flat]
	for i, flist in enumerate(files):
		for j, f in enumerate(flist):
			data = pd.read_csv(f)
			for n in names:
				axes[i, j].plot(data['# uv'] / 1000, data[n], label=n.split('_')[0])
			axes[i, j].set_title(titles[i][j], fontsize=12)
			axes[i, j].set_xlim(xlims[i][j])
			axes[i, j].set_ylim(ylims[i][j])

	axes[0, 2].legend()
	
	plt.tight_layout()
	plt.savefig(dir + '/complexity-plot.pdf', dpi=300)
	plt.close()

if __name__=='__main__':
	if len(sys.argv) > 1:
		dir = sys.argv[1]
	else:
		dir = '.'
	try:
		make_model_figure(dir)
		make_uv_plots(dir)
		make_sd_uv(dir)
	except:
		pass
	make_sd_plots(dir)
	make_iter_plots(dir)
	make_ms_plots(dir)

	
