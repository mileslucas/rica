import time
import argparse

from pipeline import pipeline
from _models import models


def time_test(project, save=False, parallel=False):
	"""
	Times the pipeline for each model and saves into a csv file
	
	Parameters
	----------
	project : str
		The folder to save the simulated data into
	save : bool, optional
		If True, will save the output from pipeline (the default is False)
	parallel : bool, optional
		If True, will run the pipeline in parallel. Requires launching CASA in parallel 
		(the default is False)
	
	"""

	times = []

	for m in models.keys():
		t0 = time.clock()
		pipeline(project, m, parallel=parallel, save_data=save)
		t1 = time.clock()
		times.append(t1-t0)

	with open('timings{}.csv'.format('.parallel' if parallel else ''), 'w+') as f:
		f.write('model,time (s)\n')
		for m, t in zip(models.keys(), times):
			f.write('{},{}\n'.format(m, t))


if __name__=='__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('folder', help='The folder to save data')
	parser.add_argument('-s', '--save', action='store_true', help='Save the data')
	parser.add_argument('-p', '--parallel', action='store_true',
		help='If true, runs cleans in parallel')

	args = parser.parse_args()

	time_test(args.folder, args.save, args.parallel)
