"""
simulate.py

Miles Lucas - mdlucas@nrao.edu
"""
import argparse
import os
import numpy as np

# CASA imports
from casac import casac
from tclean import tclean
from simutil import simutil
me = casac.measures()
sm = casac.simulator()
qa = casac.quanta()
ia = casac.image()
log = casac.logsink()
cl = casac.componentlist()
rg = casac.regionmanager()

import _models as m

# Constants
Frequency = '5GHz'
center = ['J2000','322d07m45.0', '+45d00m00.0']
abstime = '2018/06/01 12:00:00'
noise = '0.001Jy'
size = [2400, 2400, 1, 1]

def simulate(project, name, modelname, parallel=False):
	"""
	Creates a measurement set, a cleaned image, and a single-dish image from a chosen model
	
	Parameters
	----------
	project : str
		The path for putting all created files. Will create of it does not exist
	name : str
		The basename for all the files
	modelname : str, one of those defined in _models
		The modelname to simulate the data from. See the respective methods for more details
	parallel : bool, optional
		If True, will run clean in parallel (the default is False)
	
	Raises
	------
	ValueError
		If the modelname given is not one of the models
	
	"""
	# Create project directory if it doesn't already exist
	os.system('mkdir -p {}'.format(project))
	os.system('mkdir -p {}/models'.format(project))
	modeldir = '{}/models/'.format(project)

	if not modelname in m.models:
		raise ValueError('Not a recognized model')

	paths = {}
	for n in m.models.keys():
		if n == 'ppd':
			continue
		paths[n] = project + '/models/' + n + '.model'

	if modelname.startswith('points') and not os.path.isdir(paths[modelname]):
		create_points_model(paths[modelname])
	elif modelname.startswith('gauss') and not os.path.isdir(paths[modelname]):
		create_gauss_model(paths[modelname])
	elif modelname.startswith('combined-sp') and not os.path.isdir(paths[modelname]):
		create_combined_sp_model(paths[modelname])
	elif modelname.startswith('combined') and not os.path.isdir(paths[modelname]):
		create_combined_model(paths[modelname])
	elif modelname.startswith('m51'):
		os.system('cp -r data/m51.model {}'.format(paths[modelname]))
	elif modelname.startswith('orion'):
		os.system('cp -r data/orion.model {}'.format(paths[modelname]))
	elif modelname.startswith('RXJ1347'):
		os.system('cp -r data/RXJ1347.model {}'.format(paths[modelname]))
                
	base = project + '/' + name
	model = paths[modelname]

	clean_dir(project, name, ms=True, cleaned=True, sd=True)

	# Create interferometer image
	ms = create_ms(base, model, config=m.models[modelname].get('config', 'd'))
	
	# Clean image and convolve model
	params = m.models[modelname]['params']
	params.update(dict(parallel=parallel))
	clean_im = clean(ms, imagename=base + '.int', **params)
	model_convolve(model, clean_im)
		
	# Create single dish image
	sd_convolve(base, model, m.models[modelname].get('sd_size', 100))


def create_points_model(filename):
	"""
	This creates an image based on four point sources with varying fluxes at one frequency.The position of the sources should be roughly quadrilateral about 20" or more apart.

	Parameters
	----------
	filename : str
		The path for saving the model
	"""
	try:
		ia.fromshape(filename, [2400, 2400, 1, 1], overwrite=True)
		cs = ia.coordsys()
		cs.setunits(['rad', 'rad', '', 'Hz'])
		cell_rad = qa.convert(qa.quantity('0.1arcsec'), 'rad')['value']
		cs.setincrement([-cell_rad, cell_rad], 'direction')
		cs.setincrement(1e9, 'spectral')
		cs.setreferencevalue([qa.convert(center[1],'rad')['value'], 
			qa.convert(center[2],'rad')['value']], type='direction')
		cs.setreferencevalue(Frequency, 'spectral')
		ia.setcoordsys(cs.torecord())
		ia.setbrightnessunit('Jy/pixel')
		cl.addcomponent(dir='J2000 322d07m45.0 +45d00m00.0', flux=0.2, 
			freq=Frequency,	shape='point')
		cl.addcomponent(dir='J2000 322d07m45.0 +44d59m20.0', flux=0.3, 
			freq=Frequency, shape='point')
		cl.addcomponent(dir='J2000 322d07m15.0 +45d00m40.0', flux=0.1, 
			freq=Frequency, shape='point')
		cl.addcomponent(dir='J2000 322d08m25.0 +45d00m00.0', flux=0.25, 
			freq=Frequency, shape='point')
		ia.modify(cl.torecord(), subtract=False)
	finally:
		ia.done()
		cs.done()
		cl.done()


def create_gauss_model(filename):
	"""
	This creates an image based on one smooth gaussian source with a width of about
	25"

	Parameters
	----------
	filename : str
		The path for saving this model
	"""	
	try:
		ia.fromshape(filename, [2400, 2400, 1, 1], overwrite=True)
		cs = ia.coordsys()
		cs.setunits(['rad', 'rad', '', 'Hz'])
		cell_rad = qa.convert(qa.quantity('0.1arcsec'), 'rad')['value']
		cs.setincrement([-cell_rad, cell_rad], 'direction')
		cs.setincrement(1e9, 'spectral')
		cs.setreferencevalue([qa.convert(center[1],'rad')['value'],
			qa.convert(center[2],'rad')['value']], type='direction')
		cs.setreferencevalue(Frequency, 'spectral')
		ia.setcoordsys(cs.torecord())
		ia.setbrightnessunit('Jy/pixel')
		cl.addcomponent(dir='J2000 322d07m45.0 +45d00m00.0', flux=1, freq=Frequency, 
			shape='gaussian', majoraxis='30arcsec', minoraxis='20arcsec', positionangle='0deg')
		ia.modify(cl.torecord(), subtract=False)
	finally:
		ia.done()
		cs.done()
		cl.done()


def create_combined_model(filename):
	"""
	This creates an image based on a combination of multiple gaussians and point sources

	Parameters
	----------
	filename : str
		The path for saving this model
	"""
	try:
		ia.fromshape(filename, [2400, 2400, 1, 1], overwrite=True)
		cs = ia.coordsys()
		cs.setunits(['rad', 'rad', '', 'Hz'])
		cell_rad = qa.convert(qa.quantity('0.1arcsec'), 'rad')['value']
		cs.setincrement([-cell_rad, cell_rad], 'direction')
		cs.setincrement(1e9, 'spectral')
		cs.setreferencevalue([qa.convert(center[1],'rad')['value'],
			qa.convert(center[2],'rad')['value']], type='direction')
		cs.setreferencevalue(Frequency, 'spectral')
		ia.setcoordsys(cs.torecord())
		ia.setbrightnessunit('Jy/pixel')
		cl.addcomponent(dir='J2000 322d07m45.0 +45d00m00.0', flux=10, freq=Frequency, 
			shape='gaussian', majoraxis='150arcsec', minoraxis='100arcsec', positionangle='270deg')
		cl.addcomponent(dir='J2000 322d06m15.0 +45d01m30.0', flux=3, freq=Frequency, 
			shape='gaussian', majoraxis='50arcsec', minoraxis='40arcsec', positionangle='90deg')
		cl.addcomponent(dir='J2000 322d07m45.0 +44d59m00.0', flux=0.4, 
			freq=Frequency, shape='point')
		cl.addcomponent(dir='J2000 322d07m25.0 +44d59m20.0', flux=0.3, 
			freq=Frequency, shape='point')
		cl.addcomponent(dir='J2000 322d06m55.0 +45d01m00.0', flux=0.1, 
			freq=Frequency, shape='point')
		cl.addcomponent(dir='J2000 322d08m25.0 +45d00m00.0', flux=0.25, 
			freq=Frequency, shape='point')
		ia.modify(cl.torecord(), subtract=False)
	finally:
		ia.done()
		cs.done()
		cl.done()

def create_combined_sp_model(filename):
	"""
	This creates an image based on a combination of multiple gaussians and point sources with spectral channels

	Parameters
	----------
	filename : str
		The path for saving this model
	"""
	try:
		ia.fromshape(filename, [2400, 2400, 1, 101], overwrite=True)
		cs = ia.coordsys()
		cs.setunits(['rad', 'rad', '', 'Hz'])
		cell_rad = qa.convert(qa.quantity('0.1arcsec'), 'rad')['value']
		cs.setincrement([-cell_rad, cell_rad], 'direction')
		cs.setincrement(1e7, 'spectral')
		cs.setreferencevalue([qa.convert(center[1],'rad')['value'],
			qa.convert(center[2],'rad')['value']], type='direction')
		cs.setreferencevalue(Frequency, 'spectral')
		ia.setcoordsys(cs.torecord())
		ia.setbrightnessunit('Jy/pixel')
                for i, f in enumerate(np.linspace(4.5, 5.5, 101)):
                        freq = '{}GHz'.format(f)
		        cl.addcomponent(dir='J2000 322d07m45.0 +45d00m00.0', flux=10, freq=freq, 
			        shape='gaussian', majoraxis='150arcsec', minoraxis='100arcsec', positionangle='270deg')
		        cl.addcomponent(dir='J2000 322d06m15.0 +45d01m30.0', flux=3, freq=freq, 
			        shape='gaussian', majoraxis='50arcsec', minoraxis='40arcsec', positionangle='90deg')
                        amp = lambda rf: np.exp(-(f*1e9 - rf*1e9)**2 / (2*5e7**2))
		        cl.addcomponent(dir='J2000 322d07m45.0 +44d59m00.0', flux=3 + 10*amp(4.75), 
			        freq=freq, shape='point')
		        cl.addcomponent(dir='J2000 322d07m25.0 +44d59m20.0', flux=2 + 20*amp(5.25), 
			        freq=freq, shape='point')
		        cl.addcomponent(dir='J2000 322d06m55.0 +45d01m00.0', flux=1, 
			        freq=freq, shape='point')
		        cl.addcomponent(dir='J2000 322d08m25.0 +45d00m00.0', flux=0.5, 
			        freq=freq, shape='point')
                        box = rg.box(blc=[0, 0, 0, i], trc=[2399, 2399, 0, i])
		        ia.modify(cl.torecord(), subtract=False, region=box)
                        cl.done()
	finally:
		ia.done()
		cs.done()
		cl.done()
                
def create_ms(name, model, config='d', corrupt=True):
	"""
	This will simulate visibility data based on the VLA at 5 GHz
	
	Parameters
	----------
	name : str
		The basename for the generated MS
	model : str
		The path to the model for simulating data
	config : {'a', 'b', 'c', 'd', 'bnc'}, optional
		The configuration to use for VLA antennas
	corrupt : bool, optional
		If True, will corrupt the data with simple noise (the default is True)
	
	Returns
	-------
	str
		The path to the generated MS
	"""

	outname = name + '.int.ms'
	log.post('Creating MS {}'.format(outname))
	try:
		sm.open(outname)
		u = simutil()
		configdir = os.getenv('CASAPATH').split()[0] + "/data/alma/simmos/"
		x, y, z, d, padnames, telescope, posobs = \
			u.readantenna(configdir + 'vla.{}.cfg'.format(config.lower()))
		
		sm.setconfig(telescopename=telescope, x=x, y=y, z=z, dishdiameter=d.tolist(), 
			mount=['alt-az'], antname=padnames, coordsystem='global', 
			referencelocation=posobs)
		
		sm.setspwindow(spwname='CBand', freq='4.5GHz', deltafreq='10MHz', 
			freqresolution='10MHz', nchannels=101, stokes='RR')

		sm.setfield(sourcename='source', sourcedirection=center, 
			calcode='A') 
		sm.setlimits(shadowlimit=0.001, elevationlimit='8.0deg')
		sm.setauto(autocorrwt=0.0)
		sm.settimes(integrationtime='10s', usehourangle=False, 
			referencetime=me.epoch('UTC', abstime))
		
		sm.observe('source', 'CBand', starttime='0s', stoptime='30000s')
		
		sm.setdata(spwid=1, fieldid=1)
		sm.setnoise(simplenoise=noise)
		
		sm.predict(imagename=model)			

		if corrupt:
			sm.corrupt()
	finally:
		sm.close() 

	return outname

def clean(vis, **tclean_args):
	"""
	A simple wrapper for the tclean task from CASA
	
	Parameters
	----------
	vis : str
		The path to the MS
	Returns
	-------
	str
		The cleaned image
	"""
	log.post('Cleaning the MS')
	tokens = vis.split('.')
	base = '.'.join(tokens[:-1])
	args = {
			'vis': vis,
			'imagename': base
	}
	args.update(**tclean_args)
	tclean(**args)

	return base + '.image'

def model_convolve(model, cleaned):
	"""
	Convolves the model with the restoring beam of the cleaned image
	
	Parameters
	----------
	model : str
		The path to the model
	cleaned : str
		The path to the cleaned image
	"""

	log.post('Convolving the model with restoring beam')
	# Get the restoring beam
	ia.open(cleaned)
	beam = ia.restoringbeam()
	ia.close()

	if 'beams' in beam:
		beam = beam['beams']['*0']['*0']

	# Fix issue with convolve2d input names
	beam['pa'] = beam['positionangle']
	del beam['positionangle']
	
	# Convolve the model
	ia.open(model)
	tokens = model.split('.')
	outname = '.'.join(tokens[:-1]) + '.conv'
	conv = ia.convolve2d(outname, overwrite=True, **beam)
	
	ia.close()
	conv.done()


def sd_convolve(name, model, dishsize=100, frequency=5e9):
	"""
	Convolves the model with a beam consistent with the GBT
	
	Parameters
	----------
	name : str
		Basename for the output image
	model : str
		The path to the model
	dishsize : int or float, optional
		The single dish diameter in meters (the default is 100, which mimics the GBT)
        frequency : int or float, optional
                The frequency of the observations in Hz (the default is 5e9 or 5GHz)
	Returns
	-------
	str
		The path to the SD image
	"""
	log.post('Convolving the SD image')
	ia.open(model)
	padname = model + '.pad'
	pad = ia.pad(padname, npixels=1200, overwrite=True, padmask=True)
	ia.close()
	axis = '{}rad'.format(3.66e8 / (dishsize * frequency))
	outname = name + '.sd.image'
	pad.setbrightnessunit('Jy/pixel')
	im2 = pad.convolve2d(outfile=outname, type='gaussian', 
		major=axis, minor=axis, pa='0deg', overwrite=True)
	pad.done()
	im2.addnoise(type='uniform', pars=[0, 5e-6])
	im2.done()

	return outname


def clean_dir(project, name=None, ms=False, cleaned=False, sd=False):
	"""
	Cleans the project directory of files produced by the simulation
	
	Parameters
	----------
	project : str
		The path containing the files
	name : str, optional
		The name of a specific simulation set (the default is None, which means all files of the given type will be cleaned)
	ms : bool, optional
		If True, will remove MS files (the default is False)
	cleaned : bool, optional
		If True, will remove all product images of tclean (the default is False)
	sd : bool, optional
		If True, will remove all SD images (the default is False)
	
	"""
	base = project if name is None else project + '/' + name
	if ms:
		os.system('rm -rf {}*.ms'.format(base))
	if cleaned:
		os.system('find {0} -not -name {1}*.ms -name {1}*.int.* -print0 | xargs -0 rm -rf --'.format(project, '' if name is None else name))
	if sd:
		os.system('rm -rf {}*.sd.*'.format(base))


if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='')
	parser.add_argument('project', help='The path for storing all simulated files')
	parser.add_argument('name', help='The filename prefix')
	parser.add_argument('model', 
		help="The model for image creation. One of {}".format(m.models.keys()))
	args = parser.parse_args()

	simulate(args.project, args.name, args.model)
