"""
_models.py

List of models to run in pipeline

Miles Lucas - mlucas@nrao.edu
"""
import os
from math import sqrt


# Constants
c_scale = 3.4 / 1.03
b_scale = 11.1 / 1.03
a_scale = 36.4 / 1.03

def get_imsize(num):
	"""
	Gets the next largest good number for `imsize` param of `tclean`

	This is a port from casa/synthesis/ImagerObjects/SynthesisUtilMethods::getOptimumSize
	
	Parameters
	----------
	num : int or float
		The number to round
	"""
	n = int(num)
	if n % 2 != 0:
		n += 1
	factors = get_prime_factors(n, False)
	for i, fac in enumerate(factors):
		if fac > 7:
			val = fac
			while max(get_prime_factors(val)) > 7:
				val += 1
			factors[i] = val
	newlarge = 1
	for f in factors:
		newlarge *= int(f)

	for k in range(n, newlarge, 2):
		if max(get_prime_factors(k)) < 8:
			return k
	return newlarge

def get_prime_factors(num, unique=True):
	"""
	Gets the prime factors for a number

		This is a port from casa/synthesis/ImagerObjects/SynthesisUtilMethods::primeFactors

	Parameters
	----------
	num : int or float
		 The number to factorize
	unique : bool, optional
		 If true, will return a unique list of factors (the default is True)
	Returns
	-------
	list
		 The list of prime factors
	"""
	if num == 1:
		return [1,]
	factors = []
	lastresult = num
	sqlast = int(sqrt(num)) + 1
	c = 2
	while True:
		if lastresult == 1 or c > sqlast:
			break
		sqlast = int(sqrt(lastresult)) + 1
		while True:
			if c > sqlast:
				c = lastresult
				break
			if lastresult % c == 0:
				break
			c += 1
		factors.append(c)
		lastresult /= c
	if len(factors) == 0:
		return [num,]
	if unique:
		return list(set(factors))
	else:
		return factors

def merge(dict1, dict2):
	"""
	Merges two dictionaries
	
	This only exists because casa runs python 2.7 which is dumb because in python 3.5+ I could have just used
	d = {**d1, **d2}
	"""
	d = dict1.copy()
	d.update(dict2)
	return d

default_params = {
	'imsize':64,
	'cell':'7.0arcsec',
	'niter': 100000,
	'threshold': '1e-5Jy',
	'nterms': 1
}

models = {
	'points': {
		'params': default_params,
		'config': 'd'
	},
	'points-masked': {
		'params': merge(default_params,{
			'mask': os.path.dirname(__file__) + '../data/points.mask'
		}),
		'config': 'd'
	},
	'gauss': {
		'params': default_params,
		'config': 'd'
	},
	'combined': {
		'params': default_params,
		'config': 'd'
	},
	'combined-n0': {
		'params': merge(default_params, {
			'niter': 0,
		}),
		'config': 'd'
	},
	'combined-n100': {
		'params': merge(default_params, {
			'niter': 100,
		}),
		'config': 'd'
	},
	'combined-n1000': {
		'params': merge(default_params, {
			'niter': 1000,
		}),
		'config': 'd'
	},
	'combined-ms': {
		'params': merge(default_params, {
			'deconvolver': 'multiscale',
			'scales': [0, 3, 10],
		}),
		'config': 'd'
	},
	'm51': {
		'params': merge(default_params, {
			'imsize': 128,
			'threshold': '1e-12Jy',
			'pblimit': -0.1,
		}),
		'config': 'd'
	},
	'm51-ms':{
		'params': merge(default_params, {
			'imsize': 128,
			'threshold': '1e-12Jy',
			'deconvolver': 'multiscale',
			'scales': [0, 3, 10],
			'cycleniter': 400,
			'pblimit': -0.1,
		}),
		'config': 'd'
	},
	'm51-b': {
		'params': merge(default_params, {
			'imsize': get_imsize(128 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-12Jy',
			'pblimit': -0.1,
		}),
		'config': 'b'
	},
	'm51-b-ms':{
		'params': merge(default_params, {
			'imsize': get_imsize(128 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-12Jy',
			'deconvolver': 'multiscale',
			'scales': [0, 3, 10, 30],
			'pblimit': -0.1,
		}),
		'config': 'b'
	},
	'orion-b-unmasked': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'cycleniter': 400
		}),
		'config': 'b'
	},
	'orion-b': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'cycleniter': 400
		}),
		'config': 'b'
	},
	'orion-b-n0': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'niter': 0
		}),
		'config': 'b'
	},
	'orion-b-n100': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'niter': 100
		}),
		'config': 'b'
	},
	'orion-b-n1000': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'niter': 1000,
		}),
		'config': 'b'
	},
	'orion-b-ms': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'deconvolver': 'multiscale',
			'scales': [0, 3, 10, 50],
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'cycleniter': 400
		}),
		'config': 'b'
	},
	'orion-b-25': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'cycleniter': 400
		}),
		'config': 'b',
		'sd_size': 25
	},
	'orion-b-25-n0': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'niter': 0
		}),
		'config': 'b',
		'sd_size': 25
	},
	'orion-b-25-n100': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'niter': 100
		}),
		'config': 'b',
		'sd_size': 25
	},
	'orion-b-25-n1000': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'niter': 1000
		}),
		'config': 'b',
		'sd_size': 25
	},
	'orion-b-25-ms': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * b_scale),
			'cell': '{}arcsec'.format(7.0 / b_scale),
			'threshold': '1e-9Jy',
			'mask': os.path.dirname(__file__) + '../data/orion.mask',
			'cycleniter': 400,
			'deconvolver': 'multiscale',
			'scales': [0, 3, 10, 30],
		}),
		'config': 'b',
		'sd_size': 25
	},
	'orion-c': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * c_scale),
			'cell': '{}arcsec'.format(7.0 / c_scale),
			'threshold': '1e-9Jy',
			'cycleniter': 400,
		}),
		'config': 'c'
	},
	'RXJ1347': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * a_scale),
			'cell': '{}arcsec'.format(7.0 / a_scale),
			'threshold': '1e-6Jy',
		}),
		'config': 'a'
	},
	'RXJ1347-masked': {
		'params': merge(default_params, {
			'imsize': get_imsize(64 * a_scale),
			'cell': '{}arcsec'.format(7.0 / a_scale),
			'mask': os.path.dirname(__file__) + '../data/RXJ1347.mask',
			'threshold': '1e-6Jy',
		}),
		'config': 'a'
	},
	'ppd': {
		'params': merge(default_params, {
			'imsize': 192,
			'cell': '0.01arcsec',
			'threshold': '1e-7Jy',
		})
	},
}


def save_model_params(filename='models'):
	"""
	This will create a csv and latex file for every model param.

	Parameters
	----------
	filename : str, optional
		Base for saving the files (the default is 'models', which will save 'models.csv' and 'models.tex')
	
	"""
	import pandas as pd
	df = pd.DataFrame(index = models.keys())
	df['model'] = list(map(lambda n: n.split('-')[0], models.keys()))
	df['config'] = ['VLA ' + m.get('config', 'd') for m in models.values()]
	df.loc['ppd']['config'] = 'ALMA'
	df['sd size'] = ['{} m'.format(m.get('sd_size', '100')) for m in models.values()]
	df.loc['ppd']['sd size'] = 'ACA'
	df['imsize'] = ['[{0}, {0}]'.format(m['params'].get('imsize')) for m in models.values()]
	df['cell'] = ['{:.2f}arcsec'.format(float(m['params'].get('cell').split('arcsec')[0])) for m in models.values()]
	df['niter'] = ['{}'.format(m['params'].get('niter')) for m in models.values()]
	df['cycleniter'] = ['{}'.format(m['params'].get('cycleniter', '100')) for m in models.values()]
	df['threshold'] = ['{}'.format(m['params'].get('threshold')) for m in models.values()]
	df['deconvolver'] = ['{}'.format(m['params'].get('deconvolver', 'hogbom')) for m in models.values()]
	df['scales'] = ['{}'.format(m['params'].get('scales', '')) for m in models.values()]
	df['mask'] = ['{}'.format('Yes' if m['params'].get('mask', False) else 'No') for m in models.values()]

	df.to_csv(filename + '.csv')

	# Tex file
	with open(filename + '.tex', 'w+') as f:
		for i, (ind, row) in enumerate(df.iterrows()):
			line = '{} & '.format(ind)
			for val in row[:-1]:
				line += '{} & '.format(val)
			line += '{} \\\\\n'.format(row[-1])
			if i == len(df.index) - 1:
				line = line[:-3] + '\n'
			f.write(line)
