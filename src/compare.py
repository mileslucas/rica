'''
compare.py

Miles Lucas - mdlucas@nrao.edu
'''
import numpy as np
import argparse
from scipy.optimize import least_squares
import matplotlib.pyplot as plt
import os

# CASA imports
from casac import casac
from simutil import simutil
ia = casac.image()
tb = casac.table()
log = casac.logsink()
util = simutil()

# Local imports
from _plotting import plot_image, plot_fft

lim_scale = 1.25

def compare(project, reference, image, get_fidelity=True, ms=None, plot_diag=True, diag_name=None,
		plot_results=True, results_name=None):
	"""
	The driver script for comparing two images. The images are located with a project
	directory and comprise of a reference and an test image. These images will be
	regridded to each other, then Fourier transformed and a PSD will be created for each. 
	These PSDs will be sampled on a common grid and a ratio between them as well as a
	fidelity plot will be generated.
	
	Parameters
	----------
	project : str
		The path to the folder containing the images and which new images will be placed
	reference : str
		The name of the reference or 'truth' image within the project folder
	image : str
		The name of the test image within the project folder
	get_fidelity : bool, optional
		If True, will create the fidelity image for the comparison (the default is True)
	ms : str, optional
		The visibility MS for the test image for use in calculating the maximum UV dist (the default is None)
	plot_diag : bool, optional
		If  True, will plot the diagnostic plots. See `diagnostic_plot` (default is True)
	diag_name : str, optional
		The path for saving the diagnostic plot (default is None)
	plot_results : bool, optional
		If True, will plot the results plots. See `results_plot` (the default is True)
	results_name : str, optional
		The path for saving the results plot (default is None)
	
	Returns
	-------
	tuple
		Returns the PSD power ratio as a dictionary. See `get_ratio` for more information.
		Also returns the ratio chi2 and the image chi2
	"""

	path_ref = project + '/' + reference
	path_im = project + '/' + image

	base_ref = project + '/' + '.'.join(reference.split('.')[:-1])
	base_im = project + '/' + '.'.join(image.split('.')[:-1])

	regrid_ref = regrid_im(path_ref, path_im)
	
	fidelity = get_fidelity_image(regrid_ref, path_im, base_im) if get_fidelity else None
	psd_ref = get_psd(regrid_ref, base_ref)
	psd_im = get_psd(path_im, base_im)

	ratio = get_ratio(psd_ref, psd_im)

	if ms is None:
		ms = project + '/' + '.'.join(image.split('.')[:-2]) + '.int.ms'
	if os.path.isdir(ms):
		maxuvs = get_maxUV(ms)
	else:
		maxuvs = None
	if plot_diag:
		diagnostic_plot((regrid_ref, path_im), (base_ref + '.fft', base_im + '.fft'), 
			        (psd_ref, psd_im), diag_name, maxuvs)
	
	# Get the residual from the clean image used for feather
	if 'feather' in base_im:
		base_im = base_im.replace('feather', 'int')

	res_path = base_im + '.residual' 
	if not os.path.isdir(res_path):
		res_path = None
		
	if plot_results:
		results_plot(ratio, fidelity, res_path, results_name, maxuvs)

	im_chi2 = image_chi2(regrid_ref, path_im)
	os.system('rm -rf {}'.format(regrid_ref))
	return ratio, im_chi2
	

def regrid_im(reference, image):
	"""
	Regrids image to the reference image's coordsys and shape
	
	Parameters
	----------
	reference : str
		The path to the reference image
	image : str
		The path to the test image
	Returns
	-------
	str
		The path to the regridded image
	"""

	ia.open(image)

	cs = ia.coordsys()
	shp = ia.shape()
	cs.setreferencepixel([shp[0]/2, shp[1]/2], 'direction')
	ia.close()

	ia.open(reference)

	tokens = reference.split('.')
	suffix =  '.regrid.'
	outname = '.'.join(tokens[:-1]) + suffix + tokens[-1]
	ib = ia.regrid(outfile=outname, csys=cs.torecord(), shape=shp, overwrite=True)
	
	ib.done()
	cs.done()
	ia.done()
	ia.close()

	return outname


def get_psd(image, name='test'):
	"""
	Creates a power spectrum density (PSD) from given skymap axis information and 2D amplitudes
	
	Parameters
	----------
	image : str
		The path to the image
	name : str, optional
		The base name for the saved files (the default is 'test')
	
	Returns
	-------
	dict
		A dictionary with the uvdist and power, in the keys 'uv' and 'pow', respectively.
	"""

	# Perform FFT
	ia.open(image)
	beam_area = ia.beamarea()
	if 'beams' in beam_area:
		beam_area = beam_area['beams']['*0']['*0']
	fft = name + '.fft'
	if os.path.exists(fft):
		ia.removefile(fft)
	
	ia.fft(amp=fft)
	ia.close()
	ia.open(fft)
	fftcs = ia.coordsys()
	fftdata = ia.getchunk(axes=[fftcs.findcoordinate("spectral")['pixel'][0],
		fftcs.findcoordinate("stokes")['pixel'][0]], dropdeg=True)
	
	fftsum = ia.summary()
	ia.close()
	
	power = radial_profile(fftdata)
	uvdist = np.arange(0, len(power) * fftsum['incr'][1], fftsum['incr'][1])
	psd = {
		'uv': uvdist,
		'pow': power,
		'beam': beam_area
	}
	
	return psd

def radial_profile(image, center=None):
	"""
	Gets the radial profile of the FFT or, equivalently, the PSD
	
	Parameters
	----------
	image : 2D Array
		The two dimensional image data array.
	center : 2 tuple, optional
		A tuple of length 2 that defines the central pixel (the default is None, 
		which lets the center be [image.shape() / 2])
	
	Returns
	-------
	ndarray
		The radial profile or PSD
	"""

	y, x = np.indices((image.shape))

	if center is None:
		center = np.array([x.max()/2.0, y.max()/2.0])

	r = np.hypot(x - center[0], y - center[1])
	r = r.astype(np.int)

	tbin = np.bincount(r.ravel(), image.ravel())
	nr = np.bincount(r.ravel())
	radialprofile = tbin / nr
	return radialprofile


def get_ratio(psd_ref, psd_im):
	"""
	Interpolates the psd of both images and gets the ratio of im / ref for those interpolations.

	Parameters
	----------
	psd_ref: dict
		The psd dictionary for the first image
	psd_im: dict
		The psd dictionary for the second image

	Returns
	-------
	dict
		A dictionary with the following keys and values
		uv: array-like
			The uv distance of the binned values
		pow_a: array-like
			The binned power from image a
		pow_b: array-like
			The binned power from image b
		ratio: array-like
			The PSD power ratio (im/ref) of the binned values
		err: array-like
			The pointwise error of the power ratio
	"""
	uv = psd_ref['uv']
	pow_ref = psd_ref['pow']
	pow_im = psd_im['pow']

	np.seterr(divide='warn')
	ratio = pow_im / pow_ref * psd_ref['beam']['arcsec2'] / psd_im['beam']['arcsec2']
	err = 1 / (np.mean((pow_im, pow_ref), axis=0))
	
	out =  {
		'uv': uv,
		'pow_ref': pow_ref,
		'pow_im': pow_im,
		'ratio':ratio,
		'err':err
	}

	return out

def get_fidelity_image(reference, image, name='test'):
	"""
	Calculates a fidelity image given by the following formula:
	$$ F[i,j] = \frac{\abs (Model[i,j])}{\max (\abs (Difference[i,j]), 0.7\cdot RMS(Difference)} $$
	
	Parameters
	----------
	reference : str
		The path to the reference image
	image : str
		The path to the test image
	name : str, optional
		The base name for saving new files (the default is 'test')
	
	Returns
	-------
	str
		The path of the fidelity image
	"""
	difference = name + '.diff'
	diff_im = ia.imagecalc(difference, '"{0}" - "{1}"'.format(reference, image), overwrite=True)
	diff_im.setbrightnessunit('Jy/beam')
	rms = diff_im.statistics()['rms'][0]

	if '.sd' in reference:
		name += '.sd'
	elif '.model' or '.conv' in reference:
		name += '.model'
	outname = name + '.fidelity'
	im = ia.imagecalc(outfile=outname, pixels='abs("{0}") / max(abs("{1}"), 0.7*{2})'.format(reference, difference, rms), overwrite=True)

	diff_im.close()
	im.close()
	return outname

def get_maxUV(ms):
	"""
	Gets the lowest maximum UV distance in wavelengths from the given ms

	Parameters
	----------
	ms : str
		The path to the MS
	Returns
	-------
	tuple
		The lowest, mean, and highest maximum UV in wavelengths
	"""
	tb.open(ms)
	uvw = tb.getcol('UVW')
	tb.close()
	# Drop W column
	uv = uvw[:-1]
	maxuv = np.linalg.norm(uv, axis=0).max()
	# Change from m to lambda
	tb.open(ms + '/SPECTRAL_WINDOW')
	freqs = tb.getcol('CHAN_FREQ')
	low_freq = freqs.min()
	mid_freq = freqs.mean()
	high_freq = freqs.max()
	tb.close()	
	
	return (maxuv * low_freq / 3e8, maxuv * mid_freq / 3e8, maxuv * high_freq / 3e8)



def results_plot(ratio, fidelity=None, res=None, filename=None, maxuvs=None):
	"""
	Plots the PSD power ratio and fidelity images
	
	Parameters
	----------
	ratio : dict
		The dictionary for the PSD power ratio. See `get_ratio` for more information.
	fidelity : str, optional
		The fidelity image to plot, if provided (the default is None)
	res : str, optional
		The residual image to plot, if provided (the default is None)
	filename : str, optional
		If not None, will save the figure at this path and forego showing the plot.( default is None)
	maxuvs : tuple, optional
		The maximum UV distance of the interferometry data. The output will be masked 
		above 1.5 times this value. If None, there will be no mask. (Default is None)
	"""
	data = []
	for d, n in zip([res, fidelity],['Residual', 'Fidelity']):
		if d is not None:
			data.append((d,n))
	fig, axes = plt.subplots(1, len(data)+1, figsize=(5*len(data)+6, 5))
	if len(data) > 0:
		for i, (ax, (im, name)) in enumerate(zip(axes[:-1], data)):
			plot_image(im, plot_stats=True, ax=ax, plot_rb=i)
			ax.set_title(name)
	else:
		axes = [axes,]

	# Ratio Plot
	r = axes[-1]
	scale = 0.1 / min(ratio['err'])
	limit = max(ratio['uv']) if maxuvs is None else maxuvs[1] * lim_scale
	mask = ratio['uv'] < limit
	r.errorbar(ratio['uv'][mask]/1000, ratio['ratio'][mask], yerr=scale * ratio['err'][mask], 
		fmt='ro', ecolor='0.3',)
	r.set_yscale('log')
	r.set_title('Comparison of PSD')
	r.axhline(1, ls='--', c='k')
	r.set_ylabel('PSD Power Ratio')
	r.set_xlabel('UV Dist $(k\lambda )$')
	r.set_xlim(-0.25, limit/1000 + .25)
	if maxuvs is not None:
		r.axvline(maxuvs[0]/1000, ls=':', c='.3')
		r.axvline(maxuvs[2]/1000, ls=':', c='.3')
	plt.tight_layout()
	if filename is not None:
		print 'Saving file at : ' + filename
		plt.savefig(filename)
		plt.close()
	else:
		plt.show()



def diagnostic_plot(ims, ffts, psds, filename=None, maxuvs=None):
	"""
	Produces a subgridded plot of the images, their ffts, and their PSDs
	
	Parameters
	----------
	ims : listlike
		A two-length list of the images in the order of [reference, image]
	ffts : listlike
		A two-length list of the ffts in the order of [reference, image]
	psds : listlike
		A two-length list of the PSD dicts in the order of [reference, image]
	filename : str, optional
		If not None, will save the figure at this path and forego showing the plot.( default is None)
	maxuvs : tuple, optional
		The maximum UV distance of the interferometry data. The output will be masked 
		above 1.5 times this value. If None, there will be no mask. (Default is None)
	"""


	# Plots
	fig, axes = plt.subplots(2, 3, figsize=(16, 9))

	axes[0, 0].set_title('Image')
	axes[0, 1].set_title('FFT')
	axes[0, 2].set_title('PSD')

	names = ['Reference', 'Image']

	limit = None if maxuvs is None else maxuvs[0] * lim_scale
	for i, (im , fft, psd, name) in enumerate(zip(ims, ffts, psds, names)):
		plot_image(im, ax=axes[i, 0], plot_rb=True)

		lbl= axes[i, 0].get_ylabel()
		axes[i, 0].set_ylabel(name + '\n\n' + lbl)
		plot_fft(fft, ax=axes[i, 1])
		if limit is not None:
			axes[i, 1].set_xlim(-limit/1000, limit/1000)
			axes[i, 1].set_ylim(-limit/1000, limit/1000)
			
		mask = psd['uv'] < limit if limit is not None else np.isfinite(psd['uv'])
		axes[i, 2].semilogy(psd['uv'][mask]/1000, psd['pow'][mask], c='g', ms=8, mew=0, alpha=0.5)
		axes[i, 2].set_xlabel('UV Dist ($k\lambda )$')
		axes[i, 2].set_ylabel('Power')
	
	plt.tight_layout()
	if filename is not None:
		print 'Saving file at : ' + filename
		plt.savefig(filename)
		plt.close()
	else:
		plt.show()

def image_chi2(ref, image, sig=7):
	"""
	Calculates the chi2 between the reference and test image following the formula
	$$ \chi^2 = \sum{\frac{(\text{(image > 7\sigma)} - \text{ref})^2}{|\text{ref}|}} $$
	
	Parameters
	----------
	ref : str
		The path to the reference image
	image : str
		The path to the test image
	sig : int or float, optional
		The number of sigma to reject from the test image (the default is 7)
	
	Returns
	-------
	float
		The chi2 between the two images
	"""

	ia.open(ref)
	ref_data = ia.getchunk(axes=[2, 3], dropdeg=True)
	ia.close()
	
	ia.open(image)
	im_stats = ia.statistics()
	im_data = ia.getchunk(axes=[2, 3], dropdeg=True)
	ia.close()

	mask = np.where(im_data > sig * im_stats['sigma'])
	mask_im = im_data[mask]
	mask_ref = ref_data[mask]
	chi2 = np.sum((mask_im - mask_ref)**2 / np.abs(mask_ref))

	return chi2
	

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Compare the two images by getting \
		the PSD, binning them, and getting the ratio of the two. Ideally \
		this raito should be 1.0 near the 0 uv point')
	parser.add_argument('project', help='path to the folder containing the images')
	parser.add_argument('reference', help='path to the reference image')
	parser.add_argument('image', help='path to the test image')
	parser.add_argument('--no-plot', dest='plot', action='store_false', 
		help='Does not plot the final output. Useful if no X11 display')
	parser.add_argument('--no-fidelity', dest='fid', action='store_false', help='Do not calculate and plot fidelity.')
	args = parser.parse_args()
	compare(args.project, args.reference, args.image, plot_diag=args.plot, plot_results=args.plot, get_fidelity=args.fid)
